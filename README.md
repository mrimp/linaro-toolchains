___________________________________________________________________________________________________________

                                    CROSS COMPILER TOOLCHAINS - README
___________________________________________________________________________________________________________

- The toolchains with "arm-unknown-linux-gnueabi" prefix are built for generic Cortex-A cpu and configured with
similar settings to those of the latest Linaro toolchains
- The toolchains with "arm-cortex_a8-linux-gnueabi" prefix are optimized for Cortex-A8 cpu with Neon technology
support
- The toolchains with "arm-cortex_a9-linux-gnueabi" prefix are optimized for Cortex-A9 cpu with Neon technology
support
- The toolchains with "arm-cortex_a15-linux-gnueabi" prefix are optimized for Cortex-A15 cpu with Neon technology
support
- The toolchains with "arm-eabi" prefix are sabermod toolchains optimized for Cortex-A15 cpu with Neon technology
support
These are the details on each toolchain currently available on this repo
___________________________________________________________________________________________________________

